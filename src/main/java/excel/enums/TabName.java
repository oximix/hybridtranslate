package excel.enums;

public enum TabName {
    onlyExcel("Only Excel"),
    onlyJson("Only Json"),
    onlyKeyExcelNoTranslate("Empty translations"),
    repeateKey("Reapeating Key");

    private String description;

    private TabName(String description) {
        this.description = description;
    }
    public String getDescription() {return description.toLowerCase();}

}
