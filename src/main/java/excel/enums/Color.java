package excel.enums;

public enum Color {
    green(100, 192, 112),
    blue(50, 200, 200),
    red(192, 108, 103);

    Color(int color1, int color2, int color3) {
        this.color1 = color1;
        this.color2 = color2;
        this.color3 = color3;
    }

    public int getColor1() {
        return color1;
    }

    public int getColor2() {
        return color2;
    }

    public int getColor3() {
        return color3;
    }

    private int color1;
    private int color2;
    private int color3;

    public static String getClassName() {
        return Color.class.getName();
    }
}
