package excel.enums;

public enum ColumnName {
    key(0, "Key"),
    type(1, "Translation Type"),
    translate(2, "Translate"),
    rowN(3, "Number row"),
    JSONValue(4, "JSON Value");

    private String description;
    private int id;

    private ColumnName(int id, String description) {
        this.id = id;
        this.description = description;
    }
    public String getDescription() {return description.toLowerCase();}
    public int getId() {return id;}
}
