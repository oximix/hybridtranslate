package ru.oximix.hybridtranslate.json;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ParserJson {

    private Map<String, JsonTranslate> translations;

    public Map<String, JsonTranslate> getTranslations() {
        return translations;
    }

    public void setTranslations(File json) throws FileNotFoundException {

        GetStaticTranslations Result =  new Gson().fromJson(new FileReader(json), GetStaticTranslations.class);

        Map<String, JsonTranslate> translations = new LinkedHashMap<String, JsonTranslate>();

        Set<Map.Entry<String, JsonElement>> entries = Result.getResult().entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {

            JsonObject objectChild = Result.getResult().getAsJsonObject(entry.getKey());

            Set<Map.Entry<String, JsonElement>> entriesChild = objectChild.entrySet();
            for (Map.Entry<String, JsonElement> entryChild: entriesChild) {

                JsonTranslate translate = new JsonTranslate();
                translate.setTranslate(entryChild.getValue());
                translate.setTranslationType(entry.getKey());
                translations.put(entryChild.getKey(), translate);
            }
        }
        this.translations = translations;
    }

}
