package ru.oximix.hybridtranslate.json;

import com.google.gson.JsonElement;

public class JsonTranslate {

    private String translationType;
    protected String translate;
    protected int status;

    public String getTranslationType() {
        return translationType;
    }

    public void setTranslationType(String translationType) {
        this.translationType = translationType;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(JsonElement translate) {
        try{
            this.translate = translate.getAsString();
        } catch (IllegalStateException e) {
            this.translate = translate.toString();
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
