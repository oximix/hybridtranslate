package ru.oximix.hybridtranslate.translate;

public enum Status {

    empty(0),           //есть только в активном файле
    coicidence(1),      //совпали переводы
    notCoicidence(2),   //не совпали переводы по ключам
    notTranslate(3),    //нет перевода для ключа
    repeatKey(4)        // повторяющиеся ключи
    ;

    private int status;

    private Status(int status) {
        this.status = status;
    }
    public int getStatus() {return status;}

}
