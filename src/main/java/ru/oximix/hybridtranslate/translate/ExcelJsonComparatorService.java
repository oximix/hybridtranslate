package ru.oximix.hybridtranslate.translate;

import ru.oximix.hybridtranslate.excel.ExcelTranslate;
import ru.oximix.hybridtranslate.json.JsonTranslate;

import java.util.Map;
import java.util.Set;

public class ExcelJsonComparatorService {

    public void doExcelJsonCompare (Map<String, JsonTranslate> json, Map<String, ExcelTranslate> excel) {

        Set<Map.Entry<String, ExcelTranslate>> entries = excel.entrySet();
        for (Map.Entry<String, ExcelTranslate> objExcel: entries) {

            JsonTranslate objJson = json.get(objExcel.getKey());
            if (objJson != null) {

                //и совпадают переводы по этим ключам
                if (objExcel.getValue().getTranslate() != null && objJson.getTranslate() != null) {
                    if (doKeyTransform(objExcel.getValue().getTranslate()).equals(doKeyTransform(objJson.getTranslate()))) {
                        objExcel.getValue().setStatus(Status.coicidence.getStatus());
                        objJson.setStatus(Status.coicidence.getStatus());
                    } else {
                        objExcel.getValue().setStatus(Status.notCoicidence.getStatus());
                        objJson.setStatus(Status.notCoicidence.getStatus());
                    }
                }
            }
        }

        for (Map.Entry<String, ExcelTranslate> objExcel: entries) {
            //если в экселе нет перевода на ключ
            if (objExcel.getValue().getTranslate() == null) {
                objExcel.getValue().setStatus(Status.notTranslate.getStatus());
            }
        }
    }

    private String doKeyTransform (String value){
        return value.replaceAll(" ", "");
    }

}
