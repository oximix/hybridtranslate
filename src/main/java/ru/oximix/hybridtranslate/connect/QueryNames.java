package ru.oximix.hybridtranslate.connect;

public enum QueryNames {
    apiGetStaticTranslations(0, "http://clientapi-altenar-dev-v2.biahosted.com/api/Translation/GetStaticTranslations?timezoneOffset=-180&skinName=betsonic&configId=1&culture=");

    private String description;
    private int id;

    private QueryNames(int id, String description) {
        this.id = id;
        this.description = description;
    }
    public String getDescription() {return description.toLowerCase();}
    public int getId() {return id;}

}
