package ru.oximix.hybridtranslate.connect;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetStaticTranslations {

    public void apiGetStaticTranslations(String lang, String jsonName) throws Exception {
        URL url = new URL(QueryNames.apiGetStaticTranslations.getDescription() + lang);
        BufferedInputStream bis = new BufferedInputStream(url.openStream());
        FileOutputStream fis = new FileOutputStream(jsonName);
        byte[] buffer = new byte[1024];
        int count=0;
        while((count = bis.read(buffer,0,1024)) != -1)
        {
            fis.write(buffer, 0, count);
        }
        fis.close();
        bis.close();
    }
}
