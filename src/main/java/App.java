import ru.oximix.hybridtranslate.connect.GetStaticTranslations;
import ru.oximix.hybridtranslate.excel.OutputOfFile;
import ru.oximix.hybridtranslate.excel.ParserExcel;
import ru.oximix.hybridtranslate.excel.enums.TabName;
import ru.oximix.hybridtranslate.json.ParserJson;
import ru.oximix.hybridtranslate.translate.ExcelJsonComparatorService;
import ru.oximix.hybridtranslate.translate.OutputOfFileService;

import java.io.File;

public class App {

    String fileExcel;
    public String langExcel;
    String jsonName;
    public String lang;

    public App() {
        this.fileExcel = "StaticTranslations.xlsx";
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getFileExcel() {
        return fileExcel;
    }

    public void setLangExcel(String langExcel) {
        this.langExcel = langExcel;
    }

    public String getLangExcel() {
        return langExcel;
    }

    public String getJsonName() {
        return jsonName;
    }

    public void setJsonName() {
        this.jsonName = getLang() + "_response.json";
    }

    public void main() throws Exception {

        //скачать файл с переводами
        new GetStaticTranslations().apiGetStaticTranslations(getLang(), getJsonName());

        //чтение Json, что нам пислал сервер
        ParserJson json = new ParserJson();
        json.setTranslations(new File(getJsonName()));

        //чтение исходного эксель файла
        ParserExcel excel = new ParserExcel(getFileExcel());
        excel.setListTranslationsOfExcel(getLangExcel());

        //запись результатов
        OutputOfFile resultFile = new OutputOfFile(new File(getFileExcel()));

        OutputOfFileService outputOfFileService = new OutputOfFileService(resultFile);

        //вывести повторяющиеся значения
        outputOfFileService.doNewTabFillWithValue_excel(excel.getListTranslationsOfExcel(), TabName.repeateKey);

        //произвести сравнение значений
        ExcelJsonComparatorService translate = new ExcelJsonComparatorService();
        translate.doExcelJsonCompare(json.getTranslations(), excel.getListTranslationsOfExcel());

        //закрасить
        outputOfFileService.doPaintNew(excel.getListTranslationsOfExcel());

        //добавить значения для несовпавших значений
        outputOfFileService.addNotCoicidenceValue(excel, json.getTranslations());

        outputOfFileService.doNewTabFillWithValue_excel(excel.getListTranslationsOfExcel(), TabName.onlyExcel);
        outputOfFileService.doNewTabFillWithValue_excel(excel.getListTranslationsOfExcel(), TabName.onlyKeyExcelNoTranslate);
        outputOfFileService.doNewTabFillWithValue_json(json.getTranslations(), TabName.onlyJson);

    }
}
