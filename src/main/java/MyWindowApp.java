import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.GroupLayout.Alignment.BASELINE;
import static javax.swing.GroupLayout.Alignment.LEADING;

public class MyWindowApp extends JFrame { //Наследуя от JFrame мы получаем всю функциональность окна

    JLabel      labelJson;
    JTextField  textFieldJson;

    JLabel      labelExcel;
    JTextField  textFieldExcel;

    JButton     btnRun;
    JLabel      label;


    public MyWindowApp(){
        super("Translate. v1.0"); //Заголовок окна

        setBounds(200, 200, 300, 150); //Если не выставить
        //размер и положение
        //то окно будет мелкое и незаметное

//        //Подготавливаем компоненты объекта
//        labelJson = new JLabel("Язык (параметр)");
//        inputLang = new JTextArea("введите параметр языка");
//
//        labelExcel = new JLabel("Язык (название столбца)");
//        inputLangExcel = new JTextArea("введите название столбца");
//
//        run = new JButton("Run");


        labelJson               = new JLabel("Lang (parameter)");
        textFieldJson           = new JTextField("en-GB");

        labelExcel              = new JLabel("Lang (name coloumn)");
        textFieldExcel          = new JTextField("en-GB");

        btnRun                  = new JButton("Run");

        label = new JLabel();
        label.setVisible(false);

        btnRun.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {


                label.setText("Performing calculations ...");
                label.setForeground(Color.black);
                label.setVisible(true);
                btnRun.setVisible(false);

                new SwingWorker<Void, String>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        // Worken hard or hardly worken...
                        try {
                            App a = new App();
                            a.setLangExcel(textFieldExcel.getText());
                            a.setLang(textFieldJson.getText());
                            a.setJsonName();

                            //labelJson.setText(a.getJsonName());
                            //labelExcel.setText(a.getFileExcel());
                            a.main();

                            a = null;
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void done() {
                        label.setText("DONE");
                        label.setForeground(Color.BLUE);
                        label.setVisible(true);
                        btnRun.setVisible(true);
                    }
                }.execute();
            }
        });

        JPanel buttonsPanel = new JPanel(new FlowLayout());
        JPanel buttonsPane2 = new JPanel(new FlowLayout());
        JPanel buttonsPane3 = new JPanel(new FlowLayout());

        //Расставляем компоненты по местам
        buttonsPanel.add(labelJson, BorderLayout.WEST); //О размещении компонент поговорим позже
        buttonsPane2.add(labelExcel, BorderLayout.WEST); //О размещении компонент поговорим позже

        buttonsPanel.add(textFieldJson, BorderLayout.WEST);
        buttonsPane2.add(textFieldExcel, BorderLayout.EAST);
        buttonsPane3.add(btnRun);
        buttonsPane3.add(label, BorderLayout.EAST);

        add(buttonsPanel, BorderLayout.BEFORE_FIRST_LINE);
        add(buttonsPane2);
        add(buttonsPane3, BorderLayout.AFTER_LAST_LINE);

//        // Определение менеджера расположения
//        GroupLayout layout = new GroupLayout(getContentPane());
//        getContentPane().setLayout(layout);
//        layout.setAutoCreateGaps(true);
//        layout.setAutoCreateContainerGaps(true);
//
//        layout.setHorizontalGroup(layout.createSequentialGroup()
//            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER))
//
//                        .addComponent(labelJson)
//                        .addComponent(labelExcel)
//
//                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER))
//                        .addComponent(textFieldJson)
//                        .addComponent(textFieldExcel)
//            .addComponent(btnRun)
//        );
//
//
//        layout.setVerticalGroup(layout.createSequentialGroup()
//
//                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER))
//                        .addGroup(layout.createSequentialGroup()
//                            .addComponent(labelJson)
//                            .addComponent(textFieldJson))
//                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER))
//                        .addGroup(layout.createSequentialGroup()
//                            .addComponent(labelExcel)
//                            .addComponent(textFieldExcel))
//                .addComponent(btnRun)
//        );

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //это нужно для того чтобы при
        //закрытии окна закрывалась и программа,
        //иначе она останется висеть в процессах
    }

    public static void main(String[] args) { //эта функция может быть и в другом классе
        MyWindowApp app = new MyWindowApp(); //Создаем экземпляр нашего приложения
        app.setVisible(true); //С этого момента приложение запущено!
        //app.pack(); //Эта команда подбирает оптимальный размер в зависимости от содержимого окна
    }
}